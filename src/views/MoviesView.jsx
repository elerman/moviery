import React, { useContext, useState } from "react";
import { MoviesContext } from "../contexts/MoviesContext";
import MovieCard from "../components/MovieCard";
import styled from "styled-components";
import Rating from "../components/Rating";
import Pagination from "../components/Pagination";

export default () => {
  const { discover, apiConfig, setPage, searchTerm } = useContext(
    MoviesContext
  );
  const [rating, setRating] = useState(0);
  const discoverMovies = discover && discover.results;
  const page = (discover && discover.page) || 1;
  const totalPages = (discover && discover.total_pages) || 1;

  const onRatingClickHandler = (value) =>
    setRating(value === rating ? 0 : value);
  const movieCards =
    discoverMovies &&
    discoverMovies
      .filter((v) =>
        rating === 0 ? true : Math.abs(v.vote_average - rating * 2) <= 2
      )
      .map((m, i) => (
        <MovieCard movie={m} key={`mov-card-${i}`} apiConfig={apiConfig} />
      ));

  return (
    <>
      <p>Filter By Rating:</p>
      <Rating onRatingClick={onRatingClickHandler} value={rating} />
      <br /><br />
      <Pagination
        pageNumber={page}
        onPageUpdate={(p) =>
          setPage(p, searchTerm.length ? "search" : "discover")
        }
        totalPages={totalPages}
      />
      <MovieGrid>{movieCards}</MovieGrid>
      <Pagination
        pageNumber={page}
        onPageUpdate={(p) =>
          setPage(p, searchTerm.length ? "search" : "discover")
        }
        totalPages={totalPages}
      />
    </>
  );
};

const MovieGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(24%, 1fr));
  grid-gap: 14px;
  align-items: stretch;
  margin-bottom: 20px;
`;
