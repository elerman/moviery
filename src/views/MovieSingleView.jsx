import React, { useContext, useEffect, useState } from "react";
import styled from "styled-components";
import Rating from "../components/Rating";
import { MoviesContext } from "../contexts/MoviesContext";

export default (props) => {
  const [movie, setMovie] = useState(null);
  const { getMovieDetails, apiConfig } = useContext(MoviesContext);

  const imageUrl =
    (apiConfig &&
      movie &&
      movie.poster_path &&
      `${apiConfig.images.base_url}/${apiConfig.images.poster_sizes[4]}/${movie.poster_path}`) ||
    null;

  const getMovie = async () =>
    setMovie(await getMovieDetails(props.match.params.id));

  useEffect(() => {
    getMovie();
  }, []);

  return (
    movie && (
      <>
        <div className="jumbotron">
          <h1 className="display-4">{movie.title}</h1>
          <Rating value={movie.vote_average / 2} readonly={true}/>
          <p className="lead">{movie.overview}</p>
          <hr className="my-4" />
          <a
            className="btn btn-primary btn-lg"
            href={`${movie.homepage}`}
            role="button"
            target="blank"
          >
            Homepage
          </a>
          <ImageWrapper>
            {imageUrl && (
              <img src={imageUrl} className="card-img-top" alt={movie.title} />
            )}
          </ImageWrapper>
        </div>
      </>
    )
  );
};

const ImageWrapper = styled.div`
  margin: 10px 0;
`;
