import { getAPIWithKey } from "./apiKey"

export default async (id) => {
    const api = getAPIWithKey(`movie/${id}`);
    const data = await fetch(api);
    return data && await data.json();
}