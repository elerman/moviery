import { getAPIWithKey } from "./apiKey";

export default async () => {
  const apiCfg = getAPIWithKey("configuration");
  const cfg = await fetch(apiCfg);
  if (cfg) return await cfg.json();
};
