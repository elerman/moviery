import { getAPIWithKey } from "./apiKey";

export default async (searchTerm = "", page = 1) => {
  const api = `${getAPIWithKey("search/movie")}&query=${encodeURIComponent(
    searchTerm
  )}&page=${page}`;
  const searchData = await fetch(api);
  return (searchData && (await searchData.json())) || null;
};
