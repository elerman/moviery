import { getAPIWithKey } from "./apiKey";

export default async (page = 1) => {
  const api = `${getAPIWithKey("discover/movie")}&sort_by=popularity.desc&page=${page}`;
  const data = await fetch(api);
  const movies = await data.json();
  return movies;
};
