import React, { useEffect, useState } from "react";
import apiConfiguration from "../api/apiConfiguration";
import discoverMovies from "../api/discoverMovies";
import movieDetails from "../api/movieDetails";
import searchMovies from "../api/searchMovies";

export const MoviesContext = React.createContext({});

export const MoviesData = ({ children, searchTerm }) => {
  const [discover, setDiscover] = useState(null);
  const [apiConfig, setApiConfig] = useState(null);

  const fetchDiscovery = async (page) => {
    const movies = await discoverMovies(page);
    if (movies) return movies;
  };

  const setDiscovery = async (page) => setDiscover(await fetchDiscovery(page));

  const setPage = (page, context) => {
    switch (context) {
      case "search":
        moviesSearch(searchTerm, page);
        break;
      case "discover":
      default:
        setDiscovery(page);
        break;
    }
  };

  const fetchConfiguration = async () => {
    const { images } = await apiConfiguration();
    const { base_url, poster_sizes } = images;
    if (images) return { images: { base_url, poster_sizes } };
  };

  const getMovieDetails = async (movieId) => await movieDetails(movieId);

  const moviesSearch = async (searchTerm, page) =>
    setDiscover(await searchMovies(searchTerm, page));

  const initialize = async () => {
    const cfg = await fetchConfiguration();
    const discover = await fetchDiscovery();
    if (cfg) setApiConfig(cfg);
    if (discover) setDiscover(discover);
  };

  //only runs once
  useEffect(() => {
    initialize();
  }, []);

  useEffect(() => {
    (searchTerm.length && moviesSearch(searchTerm)) ||
      (discover && setDiscovery());
  }, [searchTerm]);

  const value = { discover, apiConfig, getMovieDetails, setPage, searchTerm };

  return (
    <MoviesContext.Provider value={value}>{children}</MoviesContext.Provider>
  );
};

export default MoviesData;
