import React, { useState } from "react";
import "./App.css";
import styled from "styled-components";
import Navbar from "./components/Navbar";
import MoviesData from "./contexts/MoviesContext";
import { Redirect, Route, Switch } from "react-router";
import MoviesView from "./views/MoviesView";
import { HashRouter } from "react-router-dom";
import MovieSingleView from "./views/MovieSingleView";

export default (props) => {
  const [searchTerm, setSearchTerm] = useState("");

  return (
    <AppContainer className="d-flex flex-column">
      <MoviesData searchTerm={searchTerm}>
        <HashRouter>
          <Navbar onSearch={setSearchTerm} />
          <InnerWrapper className="container">
            <Switch>
              <Route exact path="/" component={MoviesView} />
              <Route exact path="/discover" component={MoviesView} />
              <Route exact path="/search" component={MoviesView} />
              <Route exact path="/movie/:id" component={MovieSingleView} />
              <Redirect path="*" to="/" />
            </Switch>
          </InnerWrapper>
        </HashRouter>
      </MoviesData>
    </AppContainer>
  );
};

const AppContainer = styled.div``;
const InnerWrapper = styled.div`
  margin: 20px auto;
`;
