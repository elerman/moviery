import React from "react";
import Rating from "react-rating";

const noop = () => null;

export default ({ onRatingClick = noop, value, readonly }) => {
  return (
    <Rating
      emptySymbol="fa fa-star-o fa-2x medium"
      fullSymbol="fa fa-star fa-2x medium"
      onClick={onRatingClick}
      initialRating={value}
      readonly={readonly}
    />
  );
};
