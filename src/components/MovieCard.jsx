import React from "react";
import { Link } from "react-router-dom";

export default ({ movie, apiConfig }) => {
  const { images } = apiConfig;
  const imageUrl =
    (movie.poster_path &&
      `${images.base_url}/${images.poster_sizes[3]}/${movie.poster_path}`) ||
    null;
  return (
    movie &&
    apiConfig && (
      <div className="card" style={{ width: "18rem" }}>
        {imageUrl && (
          <img src={imageUrl} className="card-img-top" alt={movie.title} />
        )}
        <div className="card-body">
          <h5 className="card-title">{movie.title}</h5>
          <p className="card-text">{movie.overview}</p>
          <Link className="btn btn-primary" to={`/movie/${movie.id}`}>
            View More
          </Link>
        </div>
      </div>
    )
  );
};
