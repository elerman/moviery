import React from "react";

export default ({ pageNumber, onPageUpdate, totalPages }) => {
  return (
    <nav aria-label="Page navigation example">
      <ul className="pagination">
        {pageNumber > 1 && (
          <li className="page-item">
            <button
              className="page-link"
              onClick={() => onPageUpdate(Math.abs(pageNumber - 1))}
            >
              Previous
            </button>
          </li>
        )}
        {Array(totalPages - pageNumber + 1)
          .fill()
          .map(
            (_p, i) =>
              i < 5 && (
                <li
                  className={`page-item ${
                    pageNumber === i + pageNumber ? "active" : ""
                  }`}
                  key={i}
                >
                  <button
                    className="page-link"
                    onClick={() => onPageUpdate(i + pageNumber)}
                  >
                    {i + pageNumber}
                  </button>
                </li>
              )
          )}
        {pageNumber < totalPages && (
          <li className="page-item">
            <button
              className="page-link"
              onClick={() => onPageUpdate(pageNumber + 1)}
            >
              Next
            </button>
          </li>
        )}
      </ul>
    </nav>
  );
};
